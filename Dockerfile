FROM node:16-alpine
WORKDIR /app
COPY . /app
RUN npm install -g @angular/cli
RUN npm install -D tailwindcss
RUN apk add git
RUN git restore .
RUN git pull