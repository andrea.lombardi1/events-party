export class UserInfo {
    isLogged: boolean;
    user: any;
    events: any;


    constructor() {
        this.isLogged = false;
        this.user = {};
        this.user.withGoogle = false;
        this.user.userName = "Utente non connesso";
        this.user.userEmail = "Utente non connesso";
        this.user.userImg = "https://raw.githubusercontent.com/andrea-lombardi1/IconsEventsAngular/main/defaultImg.png";
        this.user.userUID = "qwertyuiopasdfghjklzxcvbnm12";
        this.user.createdAt = this.convertDate(0);
        this.user.lastLoginAt = this.convertDate(0);
        this.events = {};
        this.events.eventsCreated = [];
        this.events.eventsFollowed = [];
    }

    aggiornaUI(isLogged: boolean, InfoUser?: any): void {
        if (isLogged) {
            this.isLogged = true;
            this.user.withGoogle = InfoUser.user.providerData[0].providerId == "google.com";
            this.user.userName = InfoUser.user.displayName || InfoUser.user.email;
            this.user.userEmail = InfoUser.user.displayName ? InfoUser.user.email : InfoUser.user.displayName;
            this.user.userImg = InfoUser.user.photoURL || "https://raw.githubusercontent.com/andrea-lombardi1/IconsEventsAngular/main/loggedImg.png";
            this.user.userUID = InfoUser.user.uid || "qwertyuiopasdfghjklzxcvbnm12";
            this.user.createdAt = this.convertDate(Number.parseInt(InfoUser.user.createdAt)) || this.convertDate(0);
            this.user.lastLoginAt = this.convertDate(Number.parseInt(InfoUser.user.lastLoginAt)) || this.convertDate(0);
            this.events = InfoUser.events || {};
        } else {
            this.isLogged = false;
            this.user = {};
            this.user.withGoogle = false;
            this.user.userName = "Utente non connesso";
            this.user.userEmail = "Utente non connesso";
            this.user.userImg = "https://raw.githubusercontent.com/andrea-lombardi1/IconsEventsAngular/main/defaultImg.png";
            this.user.userUID = "qwertyuiopasdfghjklzxcvbnm12";
            this.user.createdAt = this.convertDate(0);
            this.user.lastLoginAt = this.convertDate(0);
            this.events = {};
            this.events.eventsCreated = [];
            this.events.eventsFollowed = [];
        }
    }
    convertDate(unix: any) {
        return new Date(new Date(unix).valueOf());
    }
}
