import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-logged',
  templateUrl: './logged.component.html',
  styleUrls: ['./logged.component.css']
})
export class LoggedComponent implements OnInit {

  @Input()userNews: any;
  @Input()isVisible: boolean;
  @Output() evLogout = new EventEmitter();
  constructor() {
    this.isVisible = false;
  }

  ngOnInit(): void {
  }

  logOut() {
    this.evLogout.emit();
  }

}
