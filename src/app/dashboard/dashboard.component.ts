import { Component, OnInit } from '@angular/core';
import { userInfo } from 'os';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  isValid: boolean = true;
  messageError: string = "Partecipa";
  eventCreationError: number = 0;
  constructor(public firebaseService: FirebaseService) {
  }

  ngOnInit(): void {
  }
  unixToDate(unixTime: number) {
    var date = new Date(new Date(unixTime).valueOf());
    return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " " + (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ":" + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
  }
  async onPartecipaSelected(event: any) {
    if (this.firebaseService.userInfo.isLogged) {
      if (event.ownerUID == this.firebaseService.userInfo.user.userUID) {
        this.isValid = false;
        this.messageError = "Sei il creatore dell'evento";
        this.eventCreationError = event.dateCreation;
      } else {
        event.follower.forEach((element: { userUID: any; ownerUID: any; }) => {
          if (element.userUID == this.firebaseService.userInfo.user.userUID) {
            this.isValid = false;
            this.messageError = "Sei già un partecipante";
            this.eventCreationError = event.dateCreation;
          }
        });
      }
      if (this.isValid) {
        await this.firebaseService.followerEvent(event);
      } else {
        this.eventError();
      }
    } else {
      this.isValid = false;
      this.messageError = "Registrati per salvare l'evento";
      this.eventCreationError = event.dateCreation;
      this.eventError();
    }
  }
  eventError() {
    setTimeout(() => {
      this.isValid = true;
      this.messageError = "Partecipa";
      this.eventCreationError = 0;
    }, 4000);
  }
  isPartecipante(event: any) :boolean {
    var existPartecipant = false;
    event.follower.forEach((element: { userUID: any; ownerUID: any; }) => {
      if (element.userUID == this.firebaseService.userInfo.user.userUID) {
        existPartecipant = true;
      }
    });
    return existPartecipant;
  }
}
