import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.css']
})
export class FormLoginComponent implements OnInit {

  errorCredential: boolean = false;
  correctCredential: boolean = false;
  invisibleDiv: boolean;
  messageTitle: string = "Accesso";
  constructor(public firebaseService: FirebaseService) {
    if (localStorage.getItem('user')) {
      this.invisibleDiv = true;
    } else {
      this.invisibleDiv = false;
    }
  }

  ngOnInit(): void {

  }

  async onSignIn(email: string, password: string) {
    this.messageTitle = "Accesso in corso...";
    await this.firebaseService.signIn(email, password);
    if (this.firebaseService.errorMessage != "") {
      this.cardCredential(false, this.firebaseService.errorMessage);
    } else {
      this.cardCredential(true, "Utente connesso");
    }
  }
  cardCredential(success: boolean, message: string) {
    if (success) {
      this.correctCredential = true;
      this.messageTitle = message;
      setTimeout(() => {
        this.invisibleDiv = true;
      }, 1000);
    } else {
      if (message.toString().includes("The email address is badly formatted.")) {
        this.messageTitle = "Formato email non valido";
      } else if (message.toString().includes("The password is invalid or the user does not have a password. (auth/wrong-password).")) {
        this.messageTitle = "Password errata";
      } else if (message.toString().includes("Error (auth/missing-email).")) {
        this.messageTitle = "Inserire l'email";
      } else if (message.toString().includes("An internal AuthError has occurred.")) {
        this.messageTitle = "Inserire la password";
      } else if (message.toString().includes("The popup has been closed by the user before finalizing the operation. (auth/popup-closed-by-user).")) {
        this.messageTitle = "Non chiudere il pop-up";
      } else if (message.toString().includes("There is no user record corresponding to this identifier. The user may have been deleted. (auth/user-not-found).")) {
        this.messageTitle = "Utente non trovato";
      } else {
        this.messageTitle = "Errore generico";
      }
      this.errorCredential = true;
      setTimeout(() => {
        this.messageTitle = "Accesso"
        this.errorCredential = false;
      }, 4000);
    }
  }

  async onSignInWithGoogle() {
    this.messageTitle = "Accesso in corso...";
    await this.firebaseService.GoogleAuth();
    if (this.firebaseService.errorMessage != "") {
      this.cardCredential(false, this.firebaseService.errorMessage);
    } else {
      this.cardCredential(true, "Utente connesso");
    }
  }
  logOut() {
    this.firebaseService.logOut();
  }
}
