import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-form-registration',
  templateUrl: './form-registration.component.html',
  styleUrls: ['./form-registration.component.css']
})
export class FormRegistrationComponent implements OnInit {

  errorCredential: boolean = false;
  correctCredential: boolean = false;
  invisibleDiv: boolean;
  messageTitle: string = "Registrazione";
  constructor(public firebaseService: FirebaseService) {
    if (localStorage.getItem('user')) {
      this.invisibleDiv = true;
    } else {
      this.invisibleDiv = false;
    }
  }

  ngOnInit(): void {

  }
  async onSignUp(email: string, password: string) {
    this.messageTitle = "Registrazione in corso...";
    await this.firebaseService.signUp(email, password);
    if (this.firebaseService.errorMessage != "") {
      this.cardCredential(false, this.firebaseService.errorMessage);
    } else {
      this.cardCredential(true, "Utente connesso");
    }
  }
  cardCredential(success: boolean, message: string) {
    if (success) {
      this.correctCredential = true;
      this.messageTitle = message;
      setTimeout(() => {
        this.invisibleDiv = true;
      }, 1000);
    } else {
      if (message.toString().includes("The email address is badly formatted.")) {
        this.messageTitle = "Formato email non valido";
      } else if (message.toString().includes("The email address is already in use by another account.")) {
        this.messageTitle = "Email già registrata";
      } else if (message.toString().includes("Password should be at least 6 characters (auth/weak-password).")) {
        this.messageTitle = "Password debole";
      } else if (message.toString().includes("Error (auth/missing-email).")) {
        this.messageTitle = "Inserire l'email";
      } else if (message.toString().includes("An internal AuthError has occurred.")) {
        this.messageTitle = "Inserire la password";
      } else if (message.toString().includes("The popup has been closed by the user before finalizing the operation. (auth/popup-closed-by-user).")) {
        this.messageTitle = "Non chiudere il pop-up";
      } else {
        this.messageTitle = "Errore generico";
      }
      this.errorCredential = true;
      setTimeout(() => {
        this.messageTitle = "Registrazione"
        this.errorCredential = false;
      }, 4000);
    }
  }

  async onSignInWithGoogle() {
    this.messageTitle = "Registrazione in corso...";
    await this.firebaseService.GoogleAuth();
    if (this.firebaseService.errorMessage != "") {
      this.cardCredential(false, this.firebaseService.errorMessage);
    } else {
      this.cardCredential(true, "Utente connesso");
    }
  }
  logOut() {
    this.firebaseService.logOut();
  }
}
