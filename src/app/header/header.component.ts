import { Component, HostListener, OnInit } from '@angular/core';
import { UserInfo } from '../class/user-info/user-info';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  scelta: boolean;
  home: boolean;
  nameSection: string;
  imgSection: string;
  firstButton: string;
  secondButton: string;
  isLogged: boolean;
  constructor(public firebaseService: FirebaseService) {
    this.scelta = false;
    this.home = false;
    this.nameSection = "Utente non connesso";
    this.imgSection = "https://raw.githubusercontent.com/andrea-lombardi1/IconsEventsAngular/main/defaultImg.png";
    this.firstButton = "Registrati";
    this.secondButton = "Accedi";
    this.isLogged = false;
    setInterval(() => {
      this.updateUI();
    }, 1);
  }

  ngOnInit(): void {
  }

  updateUI() {
    if (this.firebaseService.userInfo.isLogged) {
      this.nameSection = this.firebaseService.userInfo.user.userName || "SUS";
      this.imgSection = this.firebaseService.userInfo.user.userImg || "https://raw.githubusercontent.com/andrea-lombardi1/IconsEventsAngular/main/loggedImg.png";
      this.firstButton = "Il mio profilo";
      this.secondButton = "Esci";
      this.isLogged = true;
    } else {
      this.nameSection = "Utente non connesso";
      this.imgSection = "https://raw.githubusercontent.com/andrea-lombardi1/IconsEventsAngular/main/defaultImg.png";
      this.firstButton = "Registrati";
      this.secondButton = "Accedi";
      this.isLogged = false;
    }
  }
  menuTrigger() {
    this.updateUI();
    this.scelta = !this.scelta;
  }
  homeTrigger() {
    this.scelta = false;
    this.home = true;
    setTimeout(() => {
      this.home = false;
    }, 250);
    this.updateUI();
  }
  logTrigger() {
    if (this.firebaseService.userInfo.isLogged) {
      this.firebaseService.logOut();
    }
    this.homeTrigger();
  }
  routerLinkButton(isFirstButton: boolean): string {
    if (this.firebaseService.userInfo.isLogged) {
      if (isFirstButton) {
        return "/profile";
      } else {
        return "/";
      }
    } else {
      if (isFirstButton) {
        return "/signup";
      } else {
        return "/login";
      }
    }
  }
  @HostListener('window:scroll', ['$event'])
  onScroll() {
    this.scelta = false;
  }
}
