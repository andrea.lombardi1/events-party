import { Component, OnInit } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  imageSrc: string = "";
  userFile: any;
  viewCreate: boolean = false;
  viewEditImage: boolean = false;
  imageSelected: any;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  dateMin: string = JSON.stringify(new Date(Date.now() + 7200000));
  dateMax: string = JSON.stringify(new Date(Date.now() + 315626400000));
  errorInfos: boolean = false;
  correctInfos: boolean = false;
  messageButton: string = "Crea evento";
  eventCreated: boolean;
  constructor(public firebaseService: FirebaseService) {
    this.dateMin = (this.dateMin.split(':')[0] + ":" + this.dateMin.split(':')[1]).substring(1);
    this.dateMax = (this.dateMax.split(':')[0] + ":" + this.dateMax.split(':')[1]).substring(1);
    this.eventCreated = false;
  }

  ngOnInit(): void {
    if (this.firebaseService.userInfo.isLogged) {
      this.viewCreate = true;
    } else {
      this.viewCreate = false;
    }
  }
  onFileSelected(event: any) {
    this.imageChangedEvent = event;
    this.userFile = event.target.files[0];
    this.imageSelected = this.userFile.name;
    if (this.userFile.type != "image/png" && this.userFile.type != "image/jpeg") {
      this.imageSrc = "/assets/blank.png";
      this.croppedImage = "/assets/blank.png";
      this.messageButton = "Formato immagine non supportato";
      this.errorInfos = true;
      this.animateCard(true);
    } else if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.imageSrc = e.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
      this.viewEditImage = true;
    } else {
      this.imageSrc = "/assets/blank.png";
      this.croppedImage = "/assets/blank.png";
      this.messageButton = "Formato immagine non supportato";
      this.errorInfos = true;
      this.animateCard(true);
    }
    //const file:File = event.target.files[0];
  }
  confirmCrop() {
    this.viewEditImage = false;
  }
  deleteImg() {
    this.imageSrc = "";
  }
  async sendEvent(title: string, description: string, date: string) {
    if (this.imageSrc == '') {
      this.croppedImage = '/assets/default.png';
    }
    if (title == "") {
      this.errorInfos = true;
      this.messageButton = "Titolo mancante";
    } else if (description == "") {
      this.errorInfos = true;
      this.messageButton = "Descrizione mancante";
    } else if (date == "") {
      this.errorInfos = true;
      this.messageButton = "Data mancante";
    } else if (Date.parse(date) < Date.now()) {
      this.errorInfos = true;
      this.messageButton = "La data è passata";
    } else {
      this.messageButton = "Invio in corso...";
    }
    if (this.errorInfos) {
      this.animateCard(true);
    } else {
      await this.firebaseService.send(title, description, Date.now(), Date.parse(date), this.croppedImage);
      if (this.firebaseService.errorMessage != "") {
        this.messageButton = "Errore generico";
        this.animateCard(true);
      } else {
        this.animateCard(false);
      }
    }
  }
  animateCard(error: boolean) {
    if (error) {
      setTimeout(() => {
        this.errorInfos = false;
        this.messageButton = "Crea evento";
      }, 4000);
    } else {
      this.messageButton = "Evento creato!";
      this.correctInfos = true;
      setTimeout(() => {
        this.eventCreated = true;
      }, 1000);
    }
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
}
