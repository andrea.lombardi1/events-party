import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormLoginComponent } from './form-login/form-login.component';
import { FormRegistrationComponent } from './form-registration/form-registration.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [{
  path: "", component: DashboardComponent
},
{
  path: "login", component: FormLoginComponent
},
{
  path: "signup", component: FormRegistrationComponent
},
{
  path: "profile", component: ProfileComponent
},
{
  path: "create", component: CreateComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
