import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { FirebaseService } from './services/firebase.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FormLoginComponent } from './form-login/form-login.component';
import { FormRegistrationComponent } from './form-registration/form-registration.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { ProfileComponent } from './profile/profile.component';
import { CreateComponent } from './create/create.component';
import { LoggedComponent } from './logged/logged.component';
import { ImageCropperModule } from 'ngx-image-cropper';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FormLoginComponent,
    FormRegistrationComponent,
    DashboardComponent,
    ProfileComponent,
    CreateComponent,
    LoggedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyBBNw28C5tp7THWLWIe13UVIfpSv14cga8",
      authDomain: "events-party.firebaseapp.com",
      projectId: "events-party",
      storageBucket: "events-party.appspot.com",
      messagingSenderId: "213844212236",
      appId: "1:213844212236:web:4f6509af1c97cfbd3dcc29",
      measurementId: "G-S35XFPCBG7"
    }),
    AngularFireAuthModule,
    AngularFireStorageModule,
    HttpClientModule,
    ImageCropperModule
  ],
  providers: [FirebaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
