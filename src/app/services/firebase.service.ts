import { Injectable } from '@angular/core';
import { GoogleAuthProvider } from 'firebase/auth';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFireStorage } from "@angular/fire/compat/storage";
import firebase from 'firebase/compat';
import { initializeApp } from "firebase/app";
import { getStorage, ref, listAll, getDownloadURL, uploadBytes } from "firebase/storage";
import { HttpClient } from '@angular/common/http';
import { UserInfo } from '../class/user-info/user-info';
// Info di Firebase
const firebaseConfig = {
  apiKey: "AIzaSyBBNw28C5tp7THWLWIe13UVIfpSv14cga8",
  authDomain: "events-party.firebaseapp.com",
  projectId: "events-party",
  storageBucket: "events-party.appspot.com",
  messagingSenderId: "213844212236",
  appId: "1:213844212236:web:4f6509af1c97cfbd3dcc29",
  measurementId: "G-S35XFPCBG7"
}

const firebaseApp = initializeApp(firebaseConfig);
// Collegamento al bucket di Firebase Storage
const storage = getStorage(firebaseApp);
// Lista di ogni utente presente su Firebase
const listUsersRef = ref(storage);

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  // Contiene le informazioni dell'utente loggato in fase di ricezione dati
  public infoUser: any = {
    events: {
      eventsCreated: [],
      eventsFollowed: []
    },
    user: {}
  };

  // Contiene tutti gli eventi presenti su Firebase
  allEvents: any = [];

  // Contiene tutti gli eventi presenti su Firebase, ma ordinati in modo che quelli che scadono prima risultino in cima alla Dashboard
  allEventsSorted: any = [];

  // Contiene tutte le informazioni dell'utente loggato per gestirlo tra i vari componenti
  userInfo: UserInfo = new UserInfo();

  // In caso di problemi durante il login o la registrazione, salva il messaggio di errore
  errorMessage: string = "";

  // Limitare le richieste a Firebase quando l'utente aggiorna la Dashboard
  update: boolean;

  // Aggiorna i profili degli utenti
  updateUsers: boolean;

  // In caso di evento scaduto bisogna salvarsi l'evento per eliminarlo in tutti gli account
  eventsExpired: any = [];

  /**
   * Avvio del servizio
   * @param firebaseAuth Autenticazione degli utenti tramite Firebase Authentication
   * @param firebaseStorage Archivio di Firebase Storage
   * @param httpRequest Richiesta GET per il server di Firebase
   */
  constructor(public firebaseAuth: AngularFireAuth, public firebaseStorage: AngularFireStorage, private httpRequest: HttpClient) {

    // Disabilito l'aggiornamento dei profili degli utenti
    this.update = true;

    // Consento l'aggiornamento della Dashboard
    this.updateUsers = false;

    // Inizio la richiesta per tutti gli eventi
    this.getAllEvents();

    // Se il browser ha mantenuto le informazioni di accesso dell'utente, evita il login e passa oltre
    if (localStorage.getItem('user')) {

      // Inizio l'aggiornamento dei dati dell'utente
      this.whenUserLogged();
    }

    // Aggiorno i profili degli utenti
    this.updateProfileUsers();
  }

  errorReport(message: string) {

    // Salvo il messaggio di errore
    this.errorMessage = message;

    // Il messaggio di errore deve scomparire dopo 4 secondi per accogliere altri errori
    setTimeout(() => {
      this.errorMessage = "";
    }, 4000);
  }

  /**
   * Autenticazione con Google
   * @returns nulla di importante
   */
  async GoogleAuth() {

    // Inizializzo il Provider Google per la richiesta di login
    return await this.AuthLogin(new GoogleAuthProvider());
  }

  /**
   * Login con Google Pop-Up
   * @param provider Provider Google
   * @returns nulla di importante
   */
  async AuthLogin(provider: firebase.auth.AuthProvider) {

    return await this.firebaseAuth.signInWithPopup(provider).then((res) => {

      // Il login è avvenuto con successo; salvo i dati nella memoria del Browser
      localStorage.setItem('user', JSON.stringify(res));

      // Inizio l'aggiornamento dei dati dell'utente
      this.whenUserLogged();

    }).catch((error) => {

      // Il login non è andato a buon fine
      console.error("Errore durante il login con Google\n\n", error);

      // Avvio la procedura di errore
      this.errorReport(error);
    });
  }

  /**
   * Login con email e password
   * @param email Email dell'utente
   * @param password Password dell'utente
   */
  async signIn(email: string, password: string) {

    await this.firebaseAuth.signInWithEmailAndPassword(email, password).then(res => {

      // Il login è avvenuto con successo; salvo i dati nella memoria del Browser
      localStorage.setItem('user', JSON.stringify(res));

      // Inizio l'aggiornamento dei dati dell'utente
      this.whenUserLogged();

    }).catch((error) => {

      // Il login non è andato a buon fine
      console.error("Errore durante il login con email e password\n\n", error);

      // Avvio la procedura di errore
      this.errorReport(error);
    });
  }

  async signUp(email: string, password: string) {

    await this.firebaseAuth.createUserWithEmailAndPassword(email, password).then(res => {

      // La registrazione è avvenuta con successo; salvo i dati nella memoria del Browser
      localStorage.setItem('user', JSON.stringify(res));

      // Inizio l'aggiornamento dei dati dell'utente
      this.whenUserLogged();

    }).catch((error) => {

      // La registrazione non è andata a buon fine
      console.error("Errore durante la registrazione con email e password\n\n", error);

      // Avvio la procedura di errore
      this.errorReport(error);
    });
  }

  /**
   * Logout dall'account connesso
   */
  logOut() {

    // Logout dalla piattaforma Firebase
    this.firebaseAuth.signOut();

    // Svuoto la memoria del Browser
    localStorage.removeItem('user');

    // Aggiorno le informazioni dell'utente appena disconnesso
    this.userInfo.aggiornaUI(false);
  }

  /**
   * Creare e/o aggiornare le informazioni dell'utente su Firebase
   */
  whenUserLogged() {

    // Ricavo le informazioni dalla memoria del Browser
    var infoBrowser = JSON.parse(localStorage.getItem('user') || '{}');

    // Aggiorno le informazioni dell'utente appena connesso
    this.userInfo.aggiornaUI(true, infoBrowser);

    // Verificare se l'utente è presente in Firebase Storage
    var existingUser = false;

    // Ricavo la lista di tutti gli utenti registrati
    listAll(listUsersRef).then((res) => {

      // Ciclo tutti gli elementi presenti nel bucket di Firebase Storage
      res.items.forEach((itemRef) => {

        // Se l'utente è già presente, è inutile caricare il file di inizializzazione utente
        if (!existingUser && itemRef.name == infoBrowser.user.uid) {

          // Scarico i dati aggiornati dell'utente
          this.download(infoBrowser);

          // L'utente esiste in Firebase Storage
          existingUser = true;
        }
      });

      // Se l'utente non è presente su Firebase Storage, è necessario creare il file di nuovo utente
      if (!existingUser) {

        // Crea (in aggiunta delle informazioni personali) una sezione dedicata agli eventi
        this.createEventsObject(infoBrowser);

        // Carica i dati su Firebase Storage
        this.upload(this.infoUser);
      }
    }).catch((error) => {

      // La richiesta a Firebase Storage non è andata a buon fine
      console.error("Errore durante la lista degli elementi su Firebase\n\n", error);

      // Avvio la procedura di errore
      this.errorReport(error);
    });
  }

  /**
   * Aggiorna le informazioni dell'utente con la sezione degli eventi
   * @param infoBrowser Informazioni dell'utente
   */
  createEventsObject(infoBrowser: any) {

    // Informazioni dell'utente
    this.infoUser.user = infoBrowser.user;

    //Informazioni sugli eventi creati
    this.infoUser.events.eventsCreated = [];

    //Informazioni sugli eventi seguiti
    this.infoUser.events.eventsFollowed = [];
  }

  /**
   * Aggiornamento della Dashboard con gli eventi sincronizzati
   */
  getAllEvents() {

    // Blocco la possibilità di aggiornare la Dashboard
    this.update = false;

    // Azzero l'oggetto che conteneva i vecchi eventi per accogliere quelli nuovi
    this.allEvents.length = 0;

    // Azzero l'oggetto che conteneva i vecchi eventi ordinati per accogliere quelli nuovi, ma ordinati
    this.allEventsSorted.length = 0;

    // Ricavo la lista di tutti gli utenti registrati dove poter trovare gli eventi
    listAll(listUsersRef).then((res) => {

      // Ciclo tutti gli elementi presenti nel bucket di Firebase Storage
      res.items.forEach((itemRef) => {

        // Genero un URL da dove poter ricavare i dati interessati
        getDownloadURL(ref(storage, '/' + itemRef.name)).then((url) => {

          // Avvio la richiesta GET per scaricare i dati interessati
          this.httpRequest.get(url).subscribe(dati => {

            // I dati sono stati ricevuti correttamente
            console.log("Dati ricevuti da Firebase\n\n", dati);

            // Salvo i dati nel vettore contenente tutti gli eventi
            this.allEvents.push(dati);

            // In caso di evento scaduto bisogna confrontare gli eventi
            var data: any = [];

            // Salvo gli eventi dell'utente nella variabile
            data = JSON.parse(JSON.stringify(this.allEvents[this.allEvents.length - 1]));

            // Azzero tutti gli eventi dell'utente per poter inserire e controllare un evento alla volta
            data.events.eventsCreated.length = 0;

            // Ciclo tutti gli eventi presenti nel file dell'utente
            this.allEvents[this.allEvents.length - 1].events.eventsCreated.forEach((element: any) => {

              // Controllo che l'evento non sia scaduto
              if (element.dateExpiration >= Date.now()) {

                // Aggiungo l'evento nella sezione di ordinamento
                this.allEventsSorted.push(element);

                // Ordino l'oggetto che contiene tutti gli eventi in modo che gli eventi che scadono prima appaiono in cima alla lista
                this.allEventsSorted = this.allEventsSorted.sort((firstItem: { dateExpiration: number; }, secondItem: { dateExpiration: number; }) => firstItem.dateExpiration - secondItem.dateExpiration);

                // Aggiungo l'evento per il confronto futuro
                data.events.eventsCreated.push(element);
              } else {

                // È obbligatorio l'aggiornamento dei profili degli utenti per eliminare gli eventi scaduti
                this.updateUsers = true;

                // Aggiungo tutti i follower dell'evento per eliminarlo dal loro profilo
                this.eventsExpired.push({ uid: this.allEvents[this.allEvents.length - 1].user.uid, dateCreation: element.dateCreation, follower: element.follower });
              }
            });
            console.log("AllEvents", this.allEvents)
            // Se gli eventi dell'oggetto data sono diversi all'oggeto ricevuto da Firebase, vuol dire che qualche evento è scaduto e bisogna aggiornarlo
            if (JSON.stringify(data) != JSON.stringify(dati)) {

              // Carico i nuovi dati su Firebase Storage
              this.upload(data);

              // Scarico i file da remoto se e solo se l'evento scaduto è dell'utente loggato
              if (data.user.uid == this.infoUser.user.uid) {

                // Aggiorno i dati locali scaricandoli da Firebase Storage
                this.download(this.infoUser);
              }
            }
          });
        });
      });
    });

    // Abilito la possibilità di aggiornare la Dashboard solo dopo 30 secondi
    setTimeout(() => {
      this.update = true;
    }, 30000);
  }

  /**
   * Aggiorna i profili degli utenti interessati
   */
  updateProfileUsers() {
    setInterval(() => {
      if (this.updateUsers) {
        console.log("Aggiorno i profili")
        this.eventsExpired.forEach((infos: { uid: any; dateCreation: any; }) => {
          this.allEvents.forEach((userInfos: { events: { eventsFollowed: any[]; }; }) => {
            var isToUpdate = false;
            userInfos.events.eventsFollowed.forEach(eventFollow => {
              if (eventFollow.ownerUID == infos.uid && eventFollow.dateCreation == infos.dateCreation) {
                var index: number = userInfos.events.eventsFollowed.indexOf(eventFollow, 0);
                if (index > -1) {
                  userInfos.events.eventsFollowed.splice(index, 1);
                  isToUpdate = true;
                }
              }
            });
            if (isToUpdate) {
              this.upload(userInfos);
              isToUpdate = false;
            }
          });
        });
        this.updateUsers = false;
      }
    }, 10000);
  }

  deleteEvent(event: any) {
    this.allEvents.forEach((userInfos: { user: { uid: any; }; events: { eventsCreated: any[]; }; }) => {
      var isToUpdate = false;
      if (userInfos.user.uid == event.ownerUID) {
        userInfos.events.eventsCreated.forEach(eventCreate => {
          if (eventCreate.dateCreation == event.dateCreation) {
            eventCreate.dateExpiration = eventCreate.dateCreation;
            isToUpdate = true;
          }
        });
      }
      if (isToUpdate) {
        this.upload(userInfos);
        isToUpdate = false;
      }
    });
    this.updateUsers = true;
    this.getAllEvents();
  }

  /**
   * Procedura di Download dei dati da Firebase Storage
   * @param infoBrowser Informazioni dell'utente
   */
  download(infoBrowser: any) {

    // Preparo un riferimento al file dell'utente
    const pathUser = ref(storage, '/' + infoBrowser.user.uid);

    // Genero un URL da dove poter ricavare i dati interessati
    getDownloadURL(pathUser).then((url) => {

      // Avvio la richiesta GET per scaricare i dati interessati
      this.httpRequest.get(url).subscribe(dati => {

        // I dati sono stati ricevuti correttamente
        console.log("Dati ricevuti da Firebase\n\n", dati);

        // Salvo i dati nelle informazioni dell'utente
        this.infoUser = dati;

        // Aggiorno le informazioni dell'utente nell'interfaccia grafica
        this.userInfo.aggiornaUI(true, this.infoUser);
      });
    });
  }

  /**
   * Procedura di Upload dei dati su Firebase Storage
   * @param userData Dati da caricare
   */
  async upload(userData: any) {

    // Preparo un riferimento al file dell'utente
    const pathUser = ref(storage, '/' + userData?.user.uid);

    // Preparo il pacchetto da importare su Firebase Storage
    var blob = new Blob([JSON.stringify(userData)], { type: "application/json" });

    // Carico i dati del Blob su Firebase Storage
    uploadBytes(pathUser, blob).then(() => {

      // I dati sono stati inviati correttamente
      console.log("Dati inviati su Firebase");
    }).catch((error) => {

      // Il caricamento su Firebase Storage non è andato a buon fine
      console.error("Errore durante l'invio dei dati su Firebase\n\n", error);

      // Avvio la procedura di errore
      this.errorReport(error);
    });
  }

  /**
   * Procedura di creazione evento
   * @param titleEvent Titolo dell'evento
   * @param descriptionEvent Descrizione dell'evento
   * @param dateCreationEvent Data di creazione dell'evento
   * @param dateExpirationEvent Data di scadenza dell'evento
   * @param srcEvent Immagine dell'evento
   */
  async send(titleEvent: string, descriptionEvent: string, dateCreationEvent: number, dateExpirationEvent: number, srcEvent: string) {

    // Preparo il pacchetto dell'evento (inserisco anche alcune informazioni dell'utente e lo spazio follower)
    var infos = {
      title: titleEvent,
      description: descriptionEvent,
      dateExpiration: dateExpirationEvent,
      dateCreation: dateCreationEvent,
      image: srcEvent,
      ownerUID: this.userInfo.user.userUID,
      ownerName: this.userInfo.user.userName || this.userInfo.user.userEmail,
      ownerPhoto: this.userInfo.user.userImg,
      follower: []
    }

    // Aggiungo l'evento nella lista dell'utente
    this.infoUser.events.eventsCreated.push(infos);

    // Carico su Firebase Storage le nuove informazioni dell'utente per sincronizzarle con gli altri utenti
    await this.upload(this.infoUser);

    // Aggiorno le informazioni dell'utente nell'interfaccia grafica
    this.userInfo.aggiornaUI(true, this.infoUser);
  }
  async followerEvent(event: any) {
    this.infoUser.events.eventsFollowed.push({ ownerUID: event.ownerUID, dateCreation: event.dateCreation });
    this.upload(this.infoUser);
    this.userInfo.aggiornaUI(true, this.infoUser);
    var exit = false;
    for (let i = 0; i < this.allEvents.length && !exit; i++) {
      for (let j = 0; j < this.allEvents[i].events.eventsCreated.length && !exit; j++) {
        if (this.allEvents[i].events.eventsCreated[j].title == event.title && this.allEvents[i].events.eventsCreated[j].description == event.description && this.allEvents[i].events.eventsCreated[j].dateCreation == event.dateCreation && this.allEvents[i].events.eventsCreated[j].dateExpiration == event.dateExpiration) {
          this.allEvents[i].events.eventsCreated[j].follower.push({ userName: this.userInfo.user.userName, userImg: this.userInfo.user.userImg, userUID: this.userInfo.user.userUID });
          this.upload(this.allEvents[i]);
        }
      }
    }
  }
}