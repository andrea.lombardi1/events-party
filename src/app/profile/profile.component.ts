import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  scelta: boolean = false;
  infoSelected: boolean = true;
  createdEventsSelected: boolean = false;
  followedEventsSelected: boolean = false;
  viewProfile: boolean = false;
  eventsFollow: any = [];
  constructor(public firebaseService: FirebaseService) { }

  ngOnInit(): void {
    if (this.firebaseService.userInfo.isLogged) {
      this.viewProfile = true;
    } else {
      this.viewProfile = false;
    }
  }

  actionTrigger() {
    this.scelta = !this.scelta;
  }
  visualizza(parte: number) {
    switch (parte) {
      case 1:
        this.infoSelected = true;
        this.createdEventsSelected = false;
        this.followedEventsSelected = false;
        break;
      case 2:
        this.infoSelected = false;
        this.createdEventsSelected = true;
        this.followedEventsSelected = false;
        break;
      case 3:
        this.infoSelected = false;
        this.createdEventsSelected = false;
        this.followedEventsSelected = true;
        this.eventsFollow.length = 0;
        for (let i of this.firebaseService.userInfo.events.eventsFollowed) {
          this.infoEventFollow(i);
        }
        break;
      default:
        break;
    }
    this.actionTrigger();
  }
  infoEventFollow(eventFollow: any): any {
    this.firebaseService.allEvents.forEach((userInfos: { user: { uid: any; }; events: { eventsCreated: any[]; }; }) => {
      if (userInfos.user.uid == eventFollow.ownerUID) {
        userInfos.events.eventsCreated.forEach(eventCreate => {
          if (eventCreate.dateCreation == eventFollow.dateCreation) {
            this.eventsFollow.push(eventCreate);
          }
        });
      }
    });
  }
  lastLoginAt() {
    return this.firebaseService.userInfo.user.lastLoginAt?.getDate() + "/" + (this.firebaseService.userInfo.user.lastLoginAt?.getMonth() + 1) + "/" + this.firebaseService.userInfo.user.lastLoginAt?.getFullYear() + " " + (this.firebaseService.userInfo.user.lastLoginAt?.getHours() < 10 ? '0' + this.firebaseService.userInfo.user.lastLoginAt?.getHours() : this.firebaseService.userInfo.user.lastLoginAt?.getHours()) + ":" + (this.firebaseService.userInfo.user.lastLoginAt?.getMinutes() < 10 ? '0' + this.firebaseService.userInfo.user.lastLoginAt?.getMinutes() : this.firebaseService.userInfo.user.lastLoginAt?.getMinutes());
  }
  createdAt() {
    return this.firebaseService.userInfo.user.createdAt?.getDate() + "/" + (this.firebaseService.userInfo.user.createdAt?.getMonth() + 1) + "/" + this.firebaseService.userInfo.user.createdAt?.getFullYear() + " " + (this.firebaseService.userInfo.user.createdAt?.getHours() < 10 ? '0' + this.firebaseService.userInfo.user.createdAt?.getHours() : this.firebaseService.userInfo.user.createdAt?.getHours()) + ":" + (this.firebaseService.userInfo.user.createdAt?.getMinutes() < 10 ? '0' + this.firebaseService.userInfo.user.createdAt?.getMinutes() : this.firebaseService.userInfo.user.createdAt?.getMinutes());
  }
  unixToDate(unixTime: number) {
    var date = new Date(new Date(unixTime).valueOf());
    return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " " + (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ":" + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
  }
  onDeleteEvent(event: any) {
    this.firebaseService.deleteEvent(event);
  }
  logOut() {
    this.firebaseService.logOut();
  }
}
