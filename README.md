# Events Party - Andrea Lombardi

## Indice
* [Informazioni generali](#informazioni-generali)
* [Autenticazione](#autenticazione)
* [Grafica del sito web](#grafica-del-sito-web)
* [Git Repository](#git-repository)
* [Specifiche tecniche](#specifiche-tecniche)
* [Docker](#docker)
* [Comandi NPM](#comandi-npm)
* [Informazioni di Angular](#informazioni-di-angular)
#
## Informazioni generali
Events Party è una piattaforma online dove gli utenti possono invitare altra gente tramite un post di un evento per festeggiare in compagnia!
#
## Autenticazione
Esistono due diverse modalità di registrazione:
* Tramite email e password
* Tramite un Account Google (consigliato)

Quando l'utente sarà entrato nella piattaforma, avrà la possibilità di partecipare e/o creare gli eventi!
#
## Grafica del sito web
Per quanto riguarda la grafica, è stato utilizzato il framework TailwindCSS
#
## Git Repository
È possibile clonare la repository presente su GitLab con il seguente comando:

```
git clone https://gitlab.com/andrea.lombardi1/events-party.git
```
#
## Docker
Esiste un container Docker presente su Docker Hub da cui importare il file immagine:

```
docker run --name events-party -idt -p 80:4200 -w /app lombino/informatica:events-party
```
#
## Comandi NPM
Nel caso in cui si voglia creare il progetto da zero, ecco qui i comandi NPM da eseguire dopo l'installazione di NodeJS sul PC locale:

```
// Inizializzazione dei file per il funzionamento di NPM
npm init

// Installazione dei moduli per Angular
npm install -g @angular/cli

// Installazione delle dipendenze per TailwindCSS
npm install -D tailwindcss

// Inizializzazione del file per il funzionamento di TailwindCSS
npx tailwindcss init

// Crea il file output.css utile per la grafica del sito web
npx tailwindcss -i ./src/styles.css -o ./dist/output.css --watch

// Installazione dei moduli per Firebase
npm install firebase

// Per il funzionamento della modalità ritaglio, occorre installare ngx-image-cropper
npm install ngx-image-cropper
```
#
## Informazioni di Angular
Questo progetto è stato generato con [Angular CLI](https://github.com/angular/angular-cli) versione 13.3.1.

## Development server

L'applicazione si ricaricherà automaticamente se si modifica uno qualsiasi dei file di origine.

Esegui il comando `ng serve` per mostrare il tuo sito web all'indirizzo [`127.0.0.1`](http://localhost:4200/).

Esegui il comando `ng serve --host 0.0.0.0` per mostrare il tuo sito web all'indirizzo del PC locale [`192.168.???.???`](http://localhost:4200/).

## Generare il codice automaticamente

Esegui il comando `ng generate component 'nome-componente'` per generare un nuovo componente. Puoi anche usare `ng generate directive|pipe|service|class|guard|interface|enum|module 'nome'`.

## Build

Esegui il comando `ng build` per costruire il progetto. Gli artefatti di compilazione verranno archiviati nella directory `dist/`.

## Esecuzione di test

Esegui il comando `ng test` per eseguire gli unit test tramite [Karma](https://karma-runner.github.io).

## Esecuzione di test end-to-end

Esegui il comando `ng e2e` per eseguire i test end-to-end tramite una piattaforma a tua scelta. Per utilizzare questo comando, devi prima aggiungere un pacchetto che implementi funzionalità di test end-to-end.

## Ulteriori informazioni

Per ottenere ulteriore assistenza su Angular CLI, usa `ng help` o dai un'occhiata alla pagina [Panoramica e comando di Angular CLI](https://angular.io/cli).